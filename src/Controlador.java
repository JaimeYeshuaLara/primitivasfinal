import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import javax.swing.JButton;


public class Controlador implements ActionListener {

    private Pizarra panel;

    public Controlador(Pizarra panel) {

        this.panel = panel;

    }

    public void dibujarFiguras() {
        for (int i = 0; i < panel.aFiguras.size(); i++) {
            Figura f = panel.aFiguras.get(i);

            if (f instanceof Linea) {

                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
            }
            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarTriangulo(cd.point1, cd.point2, cd.point3,cd.color, false);
                panel.dibujarTriangulo(cd.point1, cd.point2, cd.point4,cd.color, false);
            }
            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntocenter, (Point) cr.puntoradio, cr.color, false);
            }
            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, t.color, false);
            }
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton origen = (JButton) e.getSource();

        if (origen.getName().equals("escalar")) {

            System.out.println("Escalar");
            int n = (Integer.parseInt(panel.textoNumero.getText())) - 1;

            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                double esc = Double.parseDouble(panel.textoEscalar.getText());

                transX = ((int) l.punto1.getX() - panel.origenX) * (-1);
                transY = ((int) l.punto1.getY() - panel.origenY) * (-1);

                punto[0] = l.punto1.getX();
                punto[1] = l.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = l.punto1.getX() - panel.origenX;
                punto[1] = l.punto1.getY() - panel.origenY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = l.punto2.getX() - panel.origenX;
                punto[1] = l.punto2.getY() - panel.origenY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                l.punto2.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                dibujarFiguras();

            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, panel.panelRaster.getBackground(), false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                double esc = Double.parseDouble(panel.textoEscalar.getText());

                transX = ((int) p1.x - panel.origenX) * (-1);
                transY = ((int) p1.y - panel.origenY) * (-1);

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = p1.getX() - panel.origenX;
                punto[1] = p1.getY() - panel.origenY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = p2.getX() - panel.origenX;
                punto[1] = p2.getY() - panel.origenY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                p2.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = p3.getX() - panel.origenX;
                punto[1] = p3.getY() - panel.origenY;
                punto[2] = 1;
                m2.escalar(esc);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                p3.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                Vertex2D v1 = new Vertex2D(p1.x, p1.y, t.color.getRGB());
                Vertex2D v2 = new Vertex2D(p2.x, p2.y, t.color.getRGB());
                Vertex2D v3 = new Vertex2D(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntocenter, (Point) cr.puntoradio, panel.panelRaster.getBackground(), false);
                System.out.println(cr.puntocenter.getX() + "," + cr.puntoradio.getX());
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                double esc = Double.parseDouble(panel.textoEscalar.getText());

                transX = ((int) cr.puntocenter.getX() - panel.origenX) * (-1);
                transY = ((int) cr.puntocenter.getY() - panel.origenY) * (-1);

                punto[0] = cr.puntocenter.getX();
                punto[1] = cr.puntocenter.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = cr.puntocenter.getX() - panel.origenX;
                punto[1] = cr.puntocenter.getY() - panel.origenY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                cr.puntocenter.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                punto[0] = cr.puntoradio.getX();
                punto[1] = cr.puntoradio.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = cr.puntoradio.getX() - panel.origenX;
                punto[1] = cr.puntoradio.getY() - panel.origenY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                cr.puntoradio.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                dibujarFiguras();
            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarTriangulo(cd.point1, cd.point2, cd.point3,panel.panelRaster.getBackground(), false);
                panel.dibujarTriangulo(cd.point1, cd.point2, cd.point4,panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int pxm = (int) ((int) cd.point1.getX() + cd.point2.getX()) / 2;
                int pym = (int) ((int) cd.point1.getY() + cd.point2.getY()) / 2;;
                double escalar = Double.parseDouble(panel.textoEscalar.getText());

                transX = (pxm - panel.origenX) * (-1);
                transY = (pym - panel.origenY) * (-1);

                punto[0] = cd.point1.getX();
                punto[1] = cd.point1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = cd.point1.getX() - panel.origenX;
                punto[1] = cd.point1.getY() - panel.origenY;
                punto[2] = 1;
                m.escalar(escalar);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                cd.point1.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                punto[0] = cd.point2.getX();
                punto[1] = cd.point2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = cd.point2.getX() - panel.origenX;
                punto[1] = cd.point2.getY() - panel.origenY;
                punto[2] = 1;
                m1.escalar(escalar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                cd.point2.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);
                
                punto[0] = cd.point3.getX();
                punto[1] = cd.point3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = cd.point3.getX() - panel.origenX;
                punto[1] = cd.point3.getY() - panel.origenY;
                punto[2] = 1;
                m2.escalar(escalar);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                cd.point3.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);
                
                punto[0] = cd.point3.getX();
                punto[1] = cd.point3.getY();
                punto[2] = 1;

                Matrix m3 = new Matrix();
                m3.traslacion(transX, transY);
                punto[0] = cd.point4.getX() - panel.origenX;
                punto[1] = cd.point4.getY() - panel.origenY;
                punto[2] = 1;
                m3.escalar(escalar);
                m3.traslacion(transX * (-1), transY * (-1));
                punto2 = m3.pprima(punto);
                cd.point4.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);
                dibujarFiguras();
            }

        } else if (origen.getName().equals("rotar") && panel.textoNumero.getText() != null) {

            System.out.println("Rotar");
            int n = Integer.parseInt(panel.textoNumero.getText()) - 1;
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int pmx = (int) ((int) (l.punto1.getX() + l.punto2.getX())) / 2;
                int pmy = (int) ((int) (l.punto1.getY() + l.punto2.getY())) / 2;
                int rotar = Integer.parseInt(panel.textoRotar.getText());

                transX = (pmx - panel.origenX) * (-1);
                transY = (pmy - panel.origenY) * (-1);

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = l.punto1.getX() - panel.origenX;
                punto[1] = l.punto1.getY() - panel.origenY;
                punto[2] = 1;
                m.rotacion(rotar);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = l.punto2.getX() - panel.origenX;
                punto[1] = l.punto2.getY() - panel.origenY;
                punto[2] = 1;
                m1.rotacion(rotar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                l.punto2.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);
                dibujarFiguras();
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, panel.panelRaster.getBackground(), false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int pmx = (p1.x + p2.x + p3.x) / 3;
                int pmy = (p1.y + p2.y + p3.y) / 3;
                int rotar = Integer.parseInt(panel.textoRotar.getText());

                transX = (pmx - panel.origenX) * (-1);
                transY = (pmy - panel.origenY) * (-1);

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = p1.getX() - panel.origenX;
                punto[1] = p1.getY() - panel.origenY;
                punto[2] = 1;
                m.rotacion(rotar);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = p2.getX() - panel.origenX;
                punto[1] = p2.getY() - panel.origenY;
                punto[2] = 1;
                m1.rotacion(rotar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                p2.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = p3.getX() - panel.origenX;
                punto[1] = p3.getY() - panel.origenY;
                punto[2] = 1;
                m2.rotacion(rotar);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                p3.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                Vertex2D v1 = new Vertex2D(p1.x, p1.y, t.color.getRGB());
                Vertex2D v2 = new Vertex2D(p2.x, p2.y, t.color.getRGB());
                Vertex2D v3 = new Vertex2D(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                dibujarFiguras();
            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarTriangulo(cd.point1, cd.point2, cd.point3,panel.panelRaster.getBackground(), false);
                panel.dibujarTriangulo(cd.point1, cd.point2, cd.point4,panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int pxm = (int) ((int) cd.point1.getX() + cd.point2.getX()) / 2;
                int pym = (int) ((int) cd.point1.getY() + cd.point2.getY()) / 2;;
                int rotar = Integer.parseInt(panel.textoRotar.getText());

                transX = (pxm - panel.origenX) * (-1);
                transY = (pym - panel.origenY) * (-1);

                punto[0] = cd.point1.getX();
                punto[1] = cd.point1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = cd.point1.getX() - panel.origenX;
                punto[1] = cd.point1.getY() - panel.origenY;
                punto[2] = 1;
                m.rotacion(rotar);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                cd.point1.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                punto[0] = cd.point2.getX();
                punto[1] = cd.point2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = cd.point2.getX() - panel.origenX;
                punto[1] = cd.point2.getY() - panel.origenY;
                punto[2] = 1;
                m1.rotacion(rotar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                cd.point2.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);
                
                punto[0] = cd.point3.getX();
                punto[1] = cd.point3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = cd.point3.getX() - panel.origenX;
                punto[1] = cd.point3.getY() - panel.origenY;
                punto[2] = 1;
                m2.rotacion(rotar);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                cd.point3.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);
                
                punto[0] = cd.point3.getX();
                punto[1] = cd.point3.getY();
                punto[2] = 1;

                Matrix m3 = new Matrix();
                m3.traslacion(transX, transY);
                punto[0] = cd.point4.getX() - panel.origenX;
                punto[1] = cd.point4.getY() - panel.origenY;
                punto[2] = 1;
                m3.rotacion(rotar);
                m3.traslacion(transX * (-1), transY * (-1));
                punto2 = m3.pprima(punto);
                cd.point4.setLocation(punto2[0] + panel.origenX, punto2[1] + panel.origenY);

                
                dibujarFiguras();
            }
        } else {

            System.out.println("Trasladar");
            int n = Integer.parseInt(panel.textoNumero.getText()) - 1;
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                punto[0] = l.punto1.getX();
                punto[1] = l.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                l.punto2.setLocation(punto2[0], punto2[1]);

               
                dibujarFiguras();
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, panel.panelRaster.getBackground(), false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0], punto2[1]);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p2.setLocation(punto2[0], punto2[1]);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p3.setLocation(punto2[0], punto2[1]);

                Vertex2D v1 = new Vertex2D(p1.x, p1.y, t.color.getRGB());
                Vertex2D v2 = new Vertex2D(p2.x, p2.y, t.color.getRGB());
                Vertex2D v3 = new Vertex2D(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntocenter, (Point) cr.puntoradio, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                punto[0] = cr.puntocenter.getX();
                punto[1] = cr.puntocenter.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                cr.puntocenter.setLocation(punto2[0], punto2[1]);

                punto[0] = cr.puntoradio.getX();
                punto[1] = cr.puntoradio.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                cr.puntoradio.setLocation(punto2[0], punto2[1]);
                dibujarFiguras();

            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarTriangulo(cd.point1, cd.point2, cd.point3,panel.panelRaster.getBackground(), false);
                panel.dibujarTriangulo(cd.point1, cd.point2, cd.point4,panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                

                punto[0] = cd.point1.getX();
                punto[1] = cd.point1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                cd.point1.setLocation(punto2[0], punto2[1]);

                punto[0] = cd.point2.getX();
                punto[1] = cd.point2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                cd.point2.setLocation(punto2[0], punto2[1] );
                
                punto[0] = cd.point3.getX();
                punto[1] = cd.point3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                cd.point3.setLocation(punto2[0] , punto2[1] );
                
                punto[0] = cd.point4.getX();
                punto[1] = cd.point4.getY();
                punto[2] = 1;

                Matrix m3 = new Matrix();
                m3.traslacion(transX, transY* (-1));
                punto2 = m3.pprima(punto);
                cd.point4.setLocation(punto2[0] , punto2[1] );
                dibujarFiguras();
            }

        }
    }

}
