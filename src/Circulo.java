
import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;


public class Circulo extends Figura {
    Point2D puntocenter;
    Point2D puntoradio;
    
     Circulo(Point pc,Point pr, Color _color){
         puntocenter= new Point(pc.x,pc.y);
         puntoradio= new Point(pr.x,pr.y);
         color=_color;
     }

}
